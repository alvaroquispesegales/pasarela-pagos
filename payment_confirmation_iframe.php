<?php include 'security.php' ?>

<html>
<head>
    <title>Secure Acceptance - Payment Form Example</title>
    <link rel="stylesheet" type="text/css" href="payment.css"/>


</head>
<script type="text/javascript"> 
        function myFunction(){
            var iframe = document.getElementById("myframe");  
            iframe.contentWindow.document.write("");
            console.log("limpiando el iframe")
            
        }
    </script>
<body onload="myFunction()"> 
<form id="payment_confirmation" action="https://testsecureacceptance.cybersource.com/pay" method="post" target="my-iframe"/>
<?php
    foreach($_REQUEST as $name => $value) {
        $params[$name] = $value;
    }
?>
<fieldset id="confirmation">
    <legend>Review Payment Details</legend>
    <div>
        <?php
            foreach($params as $name => $value) {
                echo "<div>";
                echo "<span class=\"fieldName\">" . $name . "</span><span class=\"fieldValue\">" . $value . "</span>";
                echo "</div>\n";
            }
        ?>
    </div>
</fieldset>
    <?php
        foreach($params as $name => $value) {
            echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
        }
        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . sign($params) . "\"/>\n";
    ?>
<input type="submit" id="submit" value="Confirm"/>
</form>
<!--<iframe   src="http://localhost/ejemplo-pago/web/boton_volver.php" 
    style="overflow:hidden;height:100%;width:100%" height="100%" width="10px"></iframe>-->
<iframe  id="myframe" name="my-iframe" src="https://testsecureacceptance.cybersource.com/pay" 
    style="overflow:hidden;height:100%;width:100%" height="100%" width="100%"></iframe>
    
</body>
</html>
