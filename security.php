<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', 'e715794dd4104650b224dc5042d867ba33eef85874ed4d67bbdd9713cd029177d1ad0569f5bf4009917b501b46786a46f8d4fee44824412b90aabff74762b4a290b1a031c1124d50829966eb1c9e0ba36363e5cc85c044a691ceb5a5ae578a3d443dbf5b19654067950db98b01711bf0a0eba4244b6245e9a72a1079eb21a26e');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
